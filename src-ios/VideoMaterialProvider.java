package sengine.platform.ios.video;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.kaigan.game31.IOSMoeLauncher;

import org.moe.natj.general.ptr.BytePtr;
import org.moe.natj.general.ptr.VoidPtr;
import org.moe.natj.objc.ObjCRuntime;

import java.nio.ByteBuffer;

import apple.avfoundation.AVAssetReader;
import apple.avfoundation.AVAssetReaderTrackOutput;
import apple.avfoundation.AVAssetTrack;
import apple.avfoundation.AVURLAsset;
import apple.avfoundation.c.AVFoundation;
import apple.avfoundation.enums.AVAssetReaderStatus;
import apple.avfoundation.protocol.AVAsynchronousKeyValueLoading;
import apple.corefoundation.c.CoreFoundation;
import apple.coremedia.c.CoreMedia;
import apple.coremedia.opaque.CMSampleBufferRef;
import apple.coremedia.struct.CMTime;
import apple.corevideo.c.CoreVideo;
import apple.corevideo.enums.Enums;
import apple.corevideo.opaque.CVBufferRef;
import apple.foundation.NSArray;
import apple.foundation.NSBundle;
import apple.foundation.NSDictionary;
import apple.foundation.NSNumber;
import apple.foundation.NSString;
import apple.foundation.NSURL;
import sengine.Sys;
import sengine.graphics2d.TextureUtils;
import sengine.materials.VideoMaterial;

/**
 * Created by admin on 24/10/2018.
 */

public class VideoMaterialProvider implements VideoMaterial.PlatformProvider {
    private static final String TAG = "VideoMaterialProvider";

    private static final int GL_BGRA_EXT = 0x80E1;

    public static void init() {
        VideoMaterial.platform = new VideoMaterialProvider();
    }

    public static class VideoMaterialHandle implements VideoMaterial.PlatformHandle {

        public final String filename;

        private AVURLAsset asset;
        private AVAssetReaderTrackOutput trackOutput;
        private AVAssetReader reader;

        private int width;
        private int height;
        private Pixmap pixmap;

        private byte[] prevBuffer;
        private double prevPosition = -1;

        private byte[] nextBuffer;
        private double nextPosition = -1;

        private byte[] fetchedBuffer;
        private double fetchedPosition = -1;

        private double uploadedPosition = -1;

        private boolean hasEnded = false;

        public int getWidth() {
            return width;
        }

        public int getHeight() {
            return height;
        }

        public double getNextPosition() {
            return nextPosition;
        }

        public double getPrevPosition() {
            return prevPosition;
        }

        public double getFetchedPosition() {
            return fetchedPosition;
        }

        public VideoMaterialHandle(String filename) {
            this.filename = filename;

            reloadVideo();
        }

        @Override
        public Texture upload(VideoMaterial material, Texture existing, float timestamp, boolean ensureLoaded) {
            fetch(timestamp);
            if(fetchedBuffer == null || fetchedPosition == uploadedPosition)
                return existing;        // nothing fetched or already uploaded
            // Else upload now
            ByteBuffer buffer = pixmap.getPixels();
            buffer.rewind();
            buffer.put(fetchedBuffer);
            buffer.rewind();
            // Else check texture
            if(existing == null) {
                // Create texture
                existing = new TextureUtils.ManualTexture();
                existing.bind();

                Gdx.gl.glTexImage2D(GL20.GL_TEXTURE_2D, 0, GL20.GL_RGBA,
                        width, height, 0,
                        GL_BGRA_EXT, GL20.GL_UNSIGNED_BYTE, pixmap.getPixels());
                existing.setFilter(material.minFilter, material.magFilter);
                existing.setWrap(material.uWrap, material.vWrap);
            }
            else {
                // Update textures
                existing.bind();
                Gdx.gl.glTexSubImage2D(GL20.GL_TEXTURE_2D, 0, 0, 0,
                        width, height,
                        GL_BGRA_EXT, GL20.GL_UNSIGNED_BYTE, pixmap.getPixels());
            }
            uploadedPosition = fetchedPosition;
            return existing;
        }

        @Override
        public Pixmap decode(float timestamp) {
            fetch(timestamp);
            if(fetchedBuffer == null)
                return null;        // nothing fetched
            ByteBuffer buffer = pixmap.getPixels();
            buffer.rewind();
            buffer.put(fetchedBuffer);
            buffer.rewind();
            // TODO: convert BGRA to RGBA
            return pixmap;
        }


        private void fetch(double position) {
            if(position < 0)
                throw new RuntimeException("Invalid position, must be >= 0");
            if(position < prevPosition)
                reloadVideo();

            if(position < nextPosition) {
                fetchedBuffer = prevBuffer;
                fetchedPosition = prevPosition;
            }
            else if(hasEnded) {
                if(nextPosition != -1) {
                    fetchedBuffer = nextBuffer;
                    fetchedPosition = nextPosition;
                }
                else if(prevPosition != -1) {
                    // video has only one frame
                    fetchedBuffer = prevBuffer;
                    fetchedPosition = prevPosition;
                }
                else {
                    // video has no frames
                    fetchedBuffer = null;
                    fetchedPosition = -1;
                }
            }
            else {
                // Else need to load next frame
                // Keep fetching frames till position
                while (position >= nextPosition && fetchNextFrame());
                // Check output
                if (position < nextPosition) {
                    fetchedBuffer = prevBuffer;
                    fetchedPosition = prevPosition;
                }
                else if (hasEnded && nextPosition != -1) {
                    fetchedBuffer = nextBuffer;
                    fetchedPosition = nextPosition;
                }
                else if(prevPosition != -1) {
                    // video has only one frame
                    fetchedBuffer = prevBuffer;
                    fetchedPosition = prevPosition;
                }
                else {
                    // video has no frames
                    fetchedBuffer = null;
                    fetchedPosition = -1;
                }
            }
        }


        private boolean fetchNextFrame() {
            while (reader.status() == AVAssetReaderStatus.Reading) {
                CMSampleBufferRef sampleBuffer = trackOutput.copyNextSampleBuffer();
                if(frameState < 1 && sampleBuffer != null) {
                    // TODO: hack to get around black frames, investigate!
                    frameState++;
                    CoreFoundation.CFRelease(sampleBuffer);
                    continue;
                }

                if (sampleBuffer != null) {
                    CVBufferRef pixelBuffer = CoreMedia.CMSampleBufferGetImageBuffer(sampleBuffer);
                    CMTime time = CoreMedia.CMSampleBufferGetOutputPresentationTimeStamp(sampleBuffer);
                    double position = CoreMedia.CMTimeGetSeconds(time);

                    CoreVideo.CVPixelBufferLockBaseAddress(pixelBuffer, 0);
                    VoidPtr ptr = CoreVideo.CVPixelBufferGetBaseAddress(pixelBuffer);
                    BytePtr bytes = ptr.getBytePtr();

                    prevPosition = nextPosition;
                    byte[] swap = prevBuffer;
                    prevBuffer = nextBuffer;
                    nextBuffer = swap;
                    nextPosition = position;
                    if(prevPosition == -1)
                        nextPosition = 0;

                    bytes.copyTo(nextBuffer, nextBuffer.length);

                    CoreVideo.CVPixelBufferUnlockBaseAddress(pixelBuffer, 0);

                    CoreFoundation.CFRelease(sampleBuffer);
                    return true;
                }
//                else
//                    Sys.error(TAG, "D4A null1\n");

            }
            hasEnded = true;
            return false;
        }

        int frameState = 0;     // TODO: hack to get around black frames, investigate!


        private void reloadVideo() {
            // Open URL
            NSURL url = NSURL.fileURLWithPath(NSBundle.mainBundle().bundlePath() + "/assets/" + filename);
            asset = AVURLAsset.alloc().initWithURLOptions(url, null);

            // Reset
            prevPosition = -1;
            nextPosition = -1;
            fetchedBuffer = null;
            fetchedPosition = -1;
            uploadedPosition = -1;
            frameState = 0;
            hasEnded = false;

            // Load video
            synchronized(this) {
                asset.loadValuesAsynchronouslyForKeysCompletionHandler((NSArray<String>) NSArray.arrayWithObject("tracks"), new AVAsynchronousKeyValueLoading.Block_loadValuesAsynchronouslyForKeysCompletionHandler() {
                    @Override
                    public void call_loadValuesAsynchronouslyForKeysCompletionHandler() {
                        AVAssetTrack track = ObjCRuntime.cast(asset.tracksWithMediaType(AVFoundation.AVMediaTypeVideo()).get(0), AVAssetTrack.class);

                        if(pixmap == null) {
                            // Buffers not allocated yet, prepare
                            // Get dimensions
                            width = (int) track.naturalSize().width();
                            height = (int) track.naturalSize().height();

                            prevBuffer = new byte[width * height * 4];
                            nextBuffer = new byte[width * height * 4];

                            pixmap = new Pixmap(width, height, Pixmap.Format.RGBA8888);
                        }

                        // Decoding parameters
                        NSString key = ObjCRuntime.cast(CoreVideo.kCVPixelBufferPixelFormatTypeKey(), NSString.class);
                        int value = Enums.kCVPixelFormatType_32BGRA;
                        NSDictionary dictionary = NSDictionary.dictionaryWithObjectForKey(NSNumber.numberWithUnsignedInt(value), key);

                        // Start decoding
                        trackOutput = AVAssetReaderTrackOutput.alloc().initWithTrackOutputSettings(track, dictionary);
                        reader = AVAssetReader.alloc().initWithAssetError(asset, null);
                        reader.addOutput(trackOutput);
                        reader.startReading();

                        // Inform done loading
                        synchronized(VideoMaterialHandle.this) {
                            VideoMaterialHandle.this.notify();
                        }
                    }
                });

                // Wait till finished
                try {
                    wait();
                } catch (InterruptedException e) {
                    throw new RuntimeException("Unexpected interrupt", e);
                }
            }
        }


        @Override
        public void dispose() {
            pixmap.dispose();
        }
    }


    @Override
    public VideoMaterial.PlatformHandle open(String filename) {
        return new VideoMaterialHandle(filename);
    }

    @Override
    public VideoMaterial.Metadata inspect(String filename) {
        VideoMaterialHandle handle = new VideoMaterialHandle(filename);
        handle.decode(Float.MAX_VALUE);
        VideoMaterial.Metadata metadata = new VideoMaterial.Metadata(handle.getWidth(), handle.getHeight(), (float)handle.getFetchedPosition());
        handle.dispose();
        return metadata;
    }
}
